//import app from '../../src/app';
import { MongoMemoryServer } from 'mongodb-memory-server';
import appFunc from '../../src/appFunc';

describe('\'users\' service', () => {
  let mongod: any;
  let app: any;

  const userInfo = {
    email: 'someone@example.com',
    password: 'supersecret'
  };

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    process.env.MONGODBURI = mongod.getUri();
    app = appFunc();
  });

  afterAll(async () => {
    await mongod.stop();
  });

  it('registered the service', () => {
    const service = app.service('users');
    expect(service).toBeTruthy();
  });

  it('creates user', async () => {
    const user = await app.service('users').create(userInfo);

    expect(user).toBeTruthy();
  });

  it('creates duplicate user', async () => {
    await expect( async () => {
      const user = await app.service('users').create(userInfo);
    }).rejects.toThrow();
  });
  
});
