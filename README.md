# feathers-vote-4

>

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/feathers-vote-4
    yarn install
    ```

3. Start your app

    ```
    export MONGODBURI={your mongodb connection string}
    export JWT_SECRET={your JWT secret}
    PORT=8080 yarn start
    ```

## Testing

Simply run `yarn test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Variables for GitLab CI/CD

This project has a `.gitlab-ci.yml` file which implements GitLab CI/CD.

You will have to set the following CI/CD variables for it to work:

* `GCP_PROJECT_ID` (Variable) - Your Google Cloud Project ID
* `GCP_SERVICE_CREDS` (File) - Your Google Cloud credentials file.
* `JWT_SECRET` (Variable) - Your JWT secret.
* `MONGODBURI` (Variable) - Your MongoDB connection string
* `PORT` (Variable) - the port to use in the "test" stage; has
        to be non-privileged, so "8080" is a good choice.

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).
